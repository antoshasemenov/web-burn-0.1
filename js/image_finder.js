function CreateImageElement(thumbWidth, thumbHeight, oImg) {
	var nPadding = 8;

	var nScale = 1;
	if (oImg.width != 0 && oImg.height != 0) {
		if (oImg.width > oImg.height) {
			nScale = thumbWidth / oImg.width;
		} else {
			nScale = thumbHeight / oImg.height;
		}
	}
	if (nScale > 1) {
		nScale = 1; // don't scale up
	}

	var width = Math.floor(oImg.width*nScale);
	var height =  Math.floor(oImg.height*nScale);

	img = document.createElement("img");
	img.width = width;
	img.height = height;
	img.style.left = (thumbWidth + nPadding - width)/2;
	img.style.top = (thumbHeight + nPadding - height)/2;
	img.src = oImg.src;

	return img
}

function onImageListReceived(images, imgListDiv) {
	if (images != null && images.length > 0) {
	
        for (var src in images) {
            var oImg = images[src];
            img = CreateImageElement(48, 48, oImg);
            img.title = chrome.i18n.getMessage("edit_image");

            itemDiv = document.createElement("div");
            itemDiv.className = "selectableItem";

/*             img.onclick = function(src) {
				return function() {
					createCanvasWiever(src);
				};
			}(oImg.src); */	
			
			srcSpan = document.createElement("div")
			srcSpan.innerHTML = oImg.src;
			srcSpan.className = "srcKeeper";
			srcSpan.onclick = function(src) {
				return function() { 
				chrome.runtime.sendMessage({message: "tab_create", url:src});	 
				};
			}(oImg.src);

			
            imgListDiv.appendChild(itemDiv);
            itemDiv.appendChild(img);
			itemDiv.appendChild(srcSpan);
		};
	}
}

function buildCurrentDocumentImageList() {
	var aImgs = document.getElementsByTagName("img");

	var aImgData = [];
	var oFoundUrls = {}
	for (var i = 0; i < aImgs.length; i++) {
		var elImg = aImgs[i];

		//if (elImg.src == location.href) continue;			   // disregard empty src urls
		if (elImg.width + elImg.height <= 20) continue;	   // disregard silly little images
		if (elImg.src in oFoundUrls) continue;			  // disregard duplicates

		var oImageData = { width:elImg.width, height:elImg.height, src:elImg.src};
		aImgData.push(oImageData);
		oFoundUrls[elImg.src] = 1;
	}

	// Sort by width*height (in reverse)
	aImgData = aImgData.sort(function(a,b) { return (b.width*b.height) - (a.width*a.height) } );
	return aImgData;
} 


function loadImages(imgListDiv) {
	// Do a screen capture
/*	   chrome.tabs.captureVisibleTab(null, {format:"jpeg", quality:20}, function(dataUrl) {
		img = new Image();
		img.onload = function() {
			imgElement = CreateImageElement(168-4*2, 100-4*2, { width:img.width, height:img.width, src:dataUrl});
			imgElement.title = chrome.i18n.getMessage("edit_screenshot");
			document.getElementById('rightPanel').appendChild(imgElement);
			document.getElementById('rightPanel').onclick = function(dataUrl) {
				return function() { onImageClick(dataUrl) };
			}(img.src);
			
		};
		img.src = dataUrl;
		
	}); */
	
	images = buildCurrentDocumentImageList();
	
	onImageListReceived(images, imgListDiv);
}