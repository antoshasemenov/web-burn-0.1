document.addEventListener('DOMContentLoaded', function () {

	chrome.browserAction.onClicked.addListener(function(tab) {
		chrome.tabs.executeScript( null, { file: "js/popup_builder.js" } );
	});	
	
	chrome.runtime.onMessage.addListener(
		function(request, sender, sendResponse) {
			if (request.message == "convert_image_url_to_data_url"){

				var canvas = document.createElement("canvas");
				var img = new Image();
				var ctx = canvas.getContext("2d");
				var oldGCO = ctx.globalCompositeOperation;
				img.src = request.url;
				img.onload = function() {
					canvas.height = img.height;
					canvas.width = img.width;
					ctx.drawImage(img, 0, 0, img.width, img.height);
					var dataURL = canvas.toDataURL();
					sendResponse({data: dataURL});
				};
				
				return true;
				
				
			};
			if (request.message == "tab_create"){
					chrome.tabs.create({ url: request.url});
			};
		}); 
});




/*	
	chrome.extension.sendMessage({text:"createView"},function(reponse){
  //This is where the stuff you want from the background page will be
  if(reponse.type == "done")
	alert("View created");
}); 
	
	
	chrome.extension.onMessage.addListener(function(message,sender,sendResponse){
		if(message.text == "getStuff")
			sendResponse({type:"test"});
	});

	
	
});
 *//*	
//	chrome.tabs.sendMessage(tab.id, function(response) {alert('response');};
	chrome.tabs.executeScript( null, { file: "image_finder.js" } );
	
	}); */